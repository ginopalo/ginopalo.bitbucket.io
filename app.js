$(document).ready(function() {
  $.getJSON("klassen.json", function(json) {
    $("#inputs").select2({
      data: json,
      width: "180px"
    });
  });

  //bind the creating of adding rows to the table
  $("#inputs").on("select2:selecting", function(e, i) {
    var leerlingen = $(e.params.args.data.leerlingen);
    var dakpan = e.params.args.data.dakpan;
    var klas = e.params.args.data.text;

    var data = {
      dakpan: dakpan,
      klas: klas
    };

    $("#dakpanheader").hide();
    initGradeHeaders(klas, dakpan);
    $.each(leerlingen, function(index, leerling) {
      addStudentRow(leerling["firstName"] + " " + leerling["lastName"], data);
    });
  });

  //init normering input
  $("#select-normering").on("change", function() {
    var id = $(this).val();
    var normering = $("#select-normering").val();
    var puntenHeaderText =
      normering == "normering-fpp" ? "Aantal fouten" : "Aantal punten";

    $("#puntenheader").html(puntenHeaderText);
    $(".normering").hide();
    $("#" + id).show();
  });

  //init remove students from table after unselecting a group
  $("#inputs").on("select2:unselecting", function(e, i) {
    var klas = e.params.args.data.text;
    removeRowsByGroupName(klas);
  });

  //bind recalculate of grade in table
  $("#total-points, #percentage").on("keyup", function() {
    $(".points").each(function() {
      var $point = $(this);
      if ($point.val().length > 0) {
        $point.trigger("keyup");
      }
    });
  });
});

function addStudentRow(leerling, data) {
  $("#tablecontent").append(
    "<tr class='studentrow data-klas='" +
      data["klas"] +
      "'><td>" +
      leerling +
      "</td><td><input style='width:100%' class='points' type='number' step='0.1'/></td><td><span class='calculated-grade'></span></td><td><span class='calculated-grade-dakpan' style='display: none'></span></td><td>" +
      data["klas"] +
      "</td></tr>"
  );
  initRows(data);
}

function removeRowsByGroupName(klas) {
  $(".studentrow[data-klas=" + klas + "").remove();
}

function initRows(data) {
  $(".points").on("keyup", function(event) {
    var points = $(this).val();
    var $row = $(this).closest("tr");
    var $gradeSpan = $row.find(".calculated-grade");

    var normeringsvorm = $("#select-normering").val();
    var normering = $("#fpp-normering").val();

    var grade = calculateGrade(
      points,
      false,
      $gradeSpan,
      normering,
      normeringsvorm
    );
    $gradeSpan.text(grade);

    //show and calculate dakpan field in table
    if (data["dakpan"]) {
      initDakpan(data["klas"], points, $row, normering, normeringsvorm);
    }

    //colors
    if (grade >= 6) {
      $gradeSpan.css("color", "green");
    } else {
      $gradeSpan.css("color", "orangered");
    }
  });
}

function calculateGrade(
  points,
  isDakpan,
  $gradeSpan,
  normering,
  normeringsvorm
) {
  var grade;
  switch (normeringsvorm) {
    case "normering-fpp":
      grade = calculateGradeFPP(points, normering);
      break;
    default:
      grade = calculateGradePercentage(points, isDakpan, $gradeSpan, normering);
      break;
  }

  grade = Math.round(grade * 10) / 10;
  if (isDakpan) {
    grade = grade * 0.8 + 2;
    grade = Math.round(grade * 10) / 10;
  }

  return grade;
}

function calculateGradePercentage(points, isDakpan, $gradeSpan, normering) {
  var totalPoints = $("#total-points").val();
  var percentage = $("#percentage").val();

  var sPoints = totalPoints * (percentage / 100);
  var grade = 0;
  if (points >= sPoints) {
    grade = 6 + (4 / (totalPoints - sPoints)) * (points - sPoints);
  } else {
    grade = 1 + (5 / sPoints) * points;
  }
  return grade;
}

function calculateGradeFPP(fouten, normering) {
  return 10 - fouten / normering;
}

function initGradeHeaders(klas, dakpan) {
  if (!dakpan) {
    $("#dakpanheader").hide();
  } else {
    $("#dakpanheader").show();
  }
  if (klas.startsWith("mh")) {
    $("#cijferheader").html("Cijfer HAVO");
    $("#dakpanheader").html("Cijfer MAVO");
  } else if (klas.startsWith("hv")) {
    $("#cijferheader").html("Cijfer VWO");
    $("#dakpanheader").html("Cijfer HAVO");
  } else if (klas.startsWith("m")) {
    $("#cijferheader").html("Cijfer MAVO");
  } else if (klas.startsWith("h")) {
    $("#cijferheader").html("Cijfer HAVO");
  }
}

function initDakpan(klas, points, $row, normering, normeringsvorm) {
  var $gradeSpan = $row.find(".calculated-grade-dakpan");
  $gradeSpan
    .show()
    .text(calculateGrade(points, true, $gradeSpan, normering, normeringsvorm));
}
